## Terracotta Server

### Run local
```
docker run -it --rm -p 8889:8889 -p 8887:8887 \
	-v "$(pwd)"/data/gfs:/images \
	-e API_PORT=8889 \
	-e IMAGE_PATTERN=/images/gfs_{variable}_shp_tif_{year}{month}{day}/gfs_{variable}_gis_24{year}{month}{day}.tif \
	registry.gitlab.com/raster-server/terracotta/terracotta-server:latest
```

### Run in K8s

Requires two secrets: one for S3 bucket for syuncing data and one for registry credentials 

```
helm install demo ./chart
```

TODO:
- Finish charts
- add gitlab-ci
- Add viewer
- Put initcontainer to load sample data
- Move routing for viewer and server from NGINX to Ingress
- Ariflow ETL - new project

