#!/bin/bash

gunicorn --bind unix:/tmp/terracotta.sock --workers 3 terracotta.client.app:app &

sleep 5

nginx -g 'daemon off;'