#!/usr/bin/env bash

REPONAME=registry.gitlab.com/raster-server/terracotta
IMAGENAME=terracotta-base
VERSION=`cat VERSION.txt` 

docker build -t ${REPONAME}/${IMAGENAME}:${VERSION} .
docker tag ${REPONAME}/${IMAGENAME}:${VERSION} ${REPONAME}/${IMAGENAME}:latest

docker push ${REPONAME}/${IMAGENAME}:${VERSION}
docker push ${REPONAME}/${IMAGENAME}:latest
